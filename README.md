# Django OpenAM REST API Auth Example
This repository contains a Django project that uses the Northwestern OpenAM
server for authentication via its REST API. That is, it is possible to use
OpenAM as an authentication backend *without* using the Apache agent.

There are essentially two steps:

1. pip install the following repositories:

    * [python-openam](https://github.com/technivore/python-openam)
    * [django-openam-auth](https://github.com/technivore/django-openam-auth)

2. Add the following settings to your Django project:

    * `AUTHENTICATION_BACKENDS = ('django_openam_auth.authentication_backends.OpenAMJSONBackend', )`
    * `OPENAM_ENDPOINT = "https://websso.it.northwestern.edu/amserver"`

This will use the standard Django authentication machinery, so make sure you
are including the proper `django.contrib.auth` views and appropriate templates,
as outlined in the
[documentation](https://docs.djangoproject.com/en/1.10/topics/auth/default/#module-django.contrib.auth.views).
